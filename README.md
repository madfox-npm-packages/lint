Nejprve je potřeba zadat yarn add @mdfx/eslint-config-clinet
Poté se objeví peerDependencies, které jsou potřeba dodat do package.json
Nasledně je zapotřebí vytvořit v kořenové složce soubor s názvem .eslintrc, do kterého se naimporotuje configurátor, (který je nastavený v 
@mdfx/eslint-config-client) náseldujícím způsobem.
{
    "extends": "@mdfx/eslint-config-client"
}

Zároveň jsem implementoval věc s názvem prettier, která se automaticky stará o správné formátovaní javascriptu. 
Stačí spustit script následovně yarn run lint --fix.


Pokud budeme chtít v budoucnu dělat změny v nastavení linteru, tak stačí upravit pouze soubor .eslintr.js v našem balíčku.
Zůstat však nutně musí kolonka extends a plugins, kde se nastavuje, jaké pluginy budeme používat a co budeme extendovat.
Také nepoužívám off, warn error, ale místo nich jsou zkratky 0, 1, 2.

Základ package není čistě z mojí hlavy, ale doplnil jsem spoustu vlastních pravidel. Naimplementoval jsem i část, která by měla řešit react
(pokud ho budeme na budoucích projektech používat.)
